package com.qa.testScript;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignUpTestScript {

	private WebDriver driver;
	private WebElement we;
	private String screenshotName;

	@Test
	public void createAccountForSingleUser() throws Exception {
		String driverPath = System.getProperty("user.dir") + "./src/test/resources/com/qa/driver/chromedriver.exe";
		// System.out.println(driverPath);
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/com/qa/driver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Login V2", title);
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		Assert.assertTrue(true);
		WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();
		String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
		String emailID = "User" + userName + "@example.com";
		System.out.println(emailID);
		we = driver.findElement(By.xpath("//input[@id='email']"));
		we.sendKeys(emailID);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
		we.click();
		we = driver.findElement(By.xpath("//div[contains(text(),'Minor Account')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='fullName']"));
		we.sendKeys("sandhya");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='surname']"));
		we.sendKeys("Rao");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='dob']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@name='occupation']/option[18]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='address']"));
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='mobile']"));
		we.sendKeys("8546247896");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//li[1]//label[1]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='verification']"));
		we.sendKeys("000000");
		Thread.sleep(4000);
		we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='src_of_fund1']/option[2]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//div[@class='row passport-select']//input[@id='passport_number']"));
		we.sendKeys("LZ115041");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@class='passport_issue_Date input-field issuedate hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='dob2']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,1000)");
		we = driver.findElement(By.xpath("//div[@class='row passport-select']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='Investors_Rate']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='IRD_Number']"));
		we.sendKeys("048448984");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='wealth_src']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step7']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		// we =
		// driver.findElement(By.xpath("//select[@id='minor_title']/option[3]"));
		// we.click();
		// Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='minorfullName']"));
		we.sendKeys("Ashwin");
		we = driver.findElement(By.xpath("//input[@id='minordob']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='minorSurName']"));
		we.sendKeys("Jain");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@class='input-field minordob hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[85]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'1')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step8']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//select[@class='OccupationMinor selectoption  otherOcc  form-group']/option[6]"));
		we.click();
		Thread.sleep(1000);

		we = driver.findElement(By.xpath("//a[@id='same-as-investor1']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step8']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='src_of_fund2']/option[1]"));
		we.click();
		Thread.sleep(1000);
		JavascriptExecutor jsx1 = (JavascriptExecutor) driver;
		jsx1.executeScript("document.getElementById('minor_birth_certificate').click()");
		Thread.sleep(5000);
		StringSelection selection = new StringSelection(
				"C:\\Program Files (x86)\\jenkins\\IRF.jpg");
		Robot rb = new Robot();
		Thread.sleep(2000);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
		Thread.sleep(2000);
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_V);
		rb.keyRelease(KeyEvent.VK_CONTROL);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step9']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='minor_IRD_Number']"));
		we.sendKeys("025317491");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step10']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='textfirst']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//li[contains(text(),'Kiwibank')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='acount_holder_name']"));
		we.sendKeys("Zain imam");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='accountNumber']"));

		we.sendKeys("9005-0764860-001");
		Thread.sleep(1000);
		JavascriptExecutor jsx11 = (JavascriptExecutor) driver;
		jsx11.executeScript("document.getElementById('attachbankfile').click()");
		Thread.sleep(5000);
		StringSelection selection1 = new StringSelection(
				"C:\\Program Files (x86)\\jenkins\\IRF.jpg");
		Robot rb1 = new Robot();
		Thread.sleep(2000);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
		Thread.sleep(2000);
		rb1.keyPress(KeyEvent.VK_CONTROL);
		rb1.keyPress(KeyEvent.VK_V);

		rb1.keyRelease(KeyEvent.VK_V);
		rb1.keyRelease(KeyEvent.VK_CONTROL);

		rb1.keyPress(KeyEvent.VK_ENTER);
		rb1.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//span[@class='checkmark']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step12']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='submit']"));
		we.click();
		Thread.sleep(10000);
		
		System.out.println(emailID);
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		we = driver.findElement(By.xpath("//button[@class='confirm']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Email']"));
		we.clear();
		we.sendKeys(emailID);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.clear();
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//button[@id='logbtn']"));
		we.click();

		String title1 = driver.getTitle();
		System.out.println(title);
		// Assert.assertEquals(title, "Beneficiary Dashboard");

		if (title == "Beneficiary Dashboard") {
			System.out.println("Minor Account" + emailID + "Successful");
		} else {
			System.out.println("Minor Account" + emailID + "UnSuccessful");
		}
		driver.quit();
	}

}
